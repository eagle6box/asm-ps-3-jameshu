define q3
	set {unsigned char}(_start) = 0xF3
        set {unsigned char}(_start+1) = 0x48
        set {unsigned char}(_start+2) = 0x0f
        set {unsigned char}(_start+3) = 0xb8
        set {unsigned char}(_start+4) = 0xd8
	set {unsigned char}(_start+5) = 0x48
	set {unsigned char}(_start+6) = 0x89
	set {unsigned char}(_start+7) = 0x1d
end
